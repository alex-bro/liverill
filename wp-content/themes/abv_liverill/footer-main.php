<?php wp_footer(); ?>
<div class="footer">
	<div class="container">
		<div class="row wrapp">
			<div class="col15-md-5">
				<div class="logo">
					<?php echo ABVFunctions::get_text_by_slug('podval') ?>
				</div>
			</div>
			<div class="col15-md-2">
				<div class="about">
					<div class="title">
						о школе
					</div>
<!--					<ul>
						<li><a href="#">наша жизнь</a></li>
						<li><a href="#">выпускники</a></li>
						<li><a href="#">выпускники</a></li>
						<li><a href="#">что изучаем</a></li>
					</ul>-->
					<?php echo wp_nav_menu(
						array(
							"theme_location" => "footer1",
						));
					?>
				</div>
			</div>
			<div class="col15-md-2">
				<div class="footer_menu">
<!--					<ul>
						<li><a href="#">родителям</a></li>
						<li><a href="#">фотогалерея</a></li>
						<li><a href="#">видеогалерея</a></li>
						<li><a href="#">статьи</a></li>
						<li><a href="#">контактЫ</a></li>
					</ul>-->
					<?php echo wp_nav_menu(
						array(
							"theme_location" => "footer2",
						));
					?>
				</div>
			</div>
			<div class="col15-md-3">
				<div class="subscription">
					<div class="title">
						подписка на новости
					</div>
					<div class="want">
						Хочешь быть в курсе событий!?
					</div>
					<form action="">
						<input type="text" placeholder="E-mail">
						<button><img src="<?php echo bloginfo('template_directory'); ?>/img/footer_plane.png" alt=""></button>
					</form>
				</div>
			</div>
			<div class="col15-md-3">
				<div class="contacts">
					<ul>
						<?php echo ABVFunctions::get_text_by_slug('adress') ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</body>
</html>
