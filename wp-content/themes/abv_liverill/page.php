<?php
get_header('main');
while ( have_posts() ) : the_post();

	include('template-parts'.DIRECTORY_SEPARATOR.'liverill-single.php');

endwhile; // End of the loop.
get_footer('main');
