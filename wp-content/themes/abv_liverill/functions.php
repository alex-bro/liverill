<?php
/**
 * abv_liverill functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package abv_liverill
 */

if ( ! function_exists( 'abv_liverill_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function abv_liverill_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on abv_liverill, use a find and replace
	 * to change 'abv_liverill' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'abv_liverill', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'abv_liverill' ),
		'top' => esc_html__( 'Top', 'abv_liverill' ),
		'top_left' => esc_html__( 'Top Left', 'abv_liverill' ),
		'footer1' => esc_html__( 'Footer1', 'abv_liverill' ),
		'footer2' => esc_html__( 'Footer2', 'abv_liverill' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'abv_liverill_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'abv_liverill_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function abv_liverill_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'abv_liverill_content_width', 640 );
}
add_action( 'after_setup_theme', 'abv_liverill_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function abv_liverill_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'abv_liverill' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'abv_liverill' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'abv_liverill_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function abv_liverill_scripts() {
	wp_register_script('bootstrap-min', get_template_directory_uri() . "/js/bootstrap.min.js",false,false,true);
	wp_register_script('owl-carousel-min', get_template_directory_uri() . "/js/owl.carousel.min.js",false,false,true);
	wp_register_script('abv_main', get_template_directory_uri() . "/js/main.js",false,false,true);

	wp_enqueue_script('jquery');
	wp_enqueue_script('bootstrap-min');
	wp_enqueue_script('owl-carousel-min');
	wp_enqueue_script('abv_main');

	wp_localize_script('abv_common', 'abv_ajax',
		array(
			'url' => admin_url('admin-ajax.php'),
			'nonce' => wp_create_nonce('abv_ajax_nonce'),
		)
	);

	wp_enqueue_style('abv_fonts', get_template_directory_uri() . '/css/fonts.css');
	wp_enqueue_style('abv_bootstrap', get_template_directory_uri() . '/css/_bootstrap.css');
	wp_enqueue_style('bootstrap_col_15', get_template_directory_uri() . '/css/bootstrap_col_15.css');
	wp_enqueue_style('owl_carousel', get_template_directory_uri() . '/css/owl.carousel.css');
	wp_enqueue_style('font-awesome', get_template_directory_uri() . '/fonts/font-awesome-4.6.1/css/font-awesome.min.css');
	wp_enqueue_style('abv_style', get_template_directory_uri() . '/css/style.css');
}
add_action( 'wp_enqueue_scripts', 'abv_liverill_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

include 'abv_autoload.php';
include 'abv_options.php';

global $abv_liverill;

$abv_liverill = new ABVLiverill();