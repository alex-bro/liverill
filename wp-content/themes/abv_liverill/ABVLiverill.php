<?php
if (!defined('ABSPATH')) exit;
class ABVLiverill
{
    public $themeDir;
    public $themeUrl;

    function __construct(){
        $this->themeDir = dirname(__FILE__);
        $this->themeUrl = get_stylesheet_directory_uri();
        // опции
        //$this->settings();

        //add_action('admin_init', [$this, 'settings']);
        // add language
        //add_action('after_setup_theme', array($this, 'addLanguage'));
        // подключаем стили
        //add_action('wp_enqueue_scripts', [$this, 'abv_load_style_script']);
        // инит темы
        add_action('init', [$this, 'init']);
        // создаем метабокс
        add_action('add_meta_boxes', [$this, 'add_meta_boxes']);
        // сохранение поста
        add_action( 'save_post', [$this, 'save_post_data'] );
        //подключение стилей в админке
        //add_action( 'admin_enqueue_scripts', [$this, 'abv_include_image_js'] );
        // поключим шоты
        //add_shortcode('abv_portfolio', [(new ABVPortfolioWidgets()), 'abv_portfolio_shortcode']);
        // изменяем список постов
        //add_filter('manage_posts_columns', [$this, 'posts_columns_id'], 5);
        //add_action('manage_posts_custom_column', [$this, 'posts_custom_id_columns'], 5, 2);
        // удаляем дефолтніе картинки медиатеки
        //add_filter('intermediate_image_sizes_advanced', [$this, 'remove_image_sizes']);
        // дозагрузка
        //add_action('wp_ajax_abv_ajax', [$this, 'abv_action_ajax_callback']);
        //add_action('wp_ajax_nopriv_abv_ajax', [$this, 'abv_action_ajax_callback']);

        // вивод позиції у адмінці для портфоліо
/*        add_filter( 'manage_edit-portfolio_columns', [$this,'add_post_columns'], 10, 1 );
        add_action( 'manage_posts_custom_column', [$this,'fill_post_columns'], 10, 1 );
        add_filter( 'manage_edit-portfolio_sortable_columns', [$this,'sort_columns'] );
        add_action( 'pre_get_posts', [$this,'admin_orderby'] );*/

        add_action('the_post', [$this, 'setViews']);
    }

    // посчет колличество посещений
    function setViews($postID) {
        if(is_singular()) {
            $key = 'abv_post_views';
            static $count = false;
            if($count) return true;
            $count = get_post_meta($postID->ID, $key, true);
            if($count===false){
                update_post_meta($postID->ID, $key, '0');
            }else{
                $count++;
                update_post_meta($postID->ID, $key, $count);
            }
        }
    }

    function init(){
        register_post_type('services',
            array(
                'labels' => array(
                    'name' => __('Services', 'portfolio'),
                    'singular_name' => __('Service', 'portfolio'),
                    'add_new' => __('Add Services', 'portfolio'),
                    'add_new_item' => __('Add new Service', 'portfolio'),
                    'edit' => __('Edit', 'portfolio'),
                    'edit_item' => __('Edit Service', 'portfolio'),
                    'new_item' => __('New Service', 'portfolio'),
                    'view' => __('View', 'portfolio'),
                    'view_item' => __('View Service', 'portfolio'),
                    'search_items' => __('Search Service', 'portfolio'),
                    'not_found' => __('Services not found', 'portfolio'),
                    'not_found_in_trash' => __('Services not found in trash', 'portfolio'),
                    'parent' => __('Parent Services', 'portfolio')
                ),
                'public' => true,
                'publicly_queryable' => false,
                'exclude_from_search' => true,
                'menu_position' => 17,
                'supports' => array('title','thumbnail','editor'),
                'taxonomies' => array(''),
                'menu_icon' => 'dashicons-admin-appearance',
                'has_archive' => true
            )
        );
        /////////////////////////////
        register_post_type('group',
            array(
                'labels' => array(
                    'name' => __('Groups', 'portfolio'),
                    'singular_name' => __('Group', 'portfolio'),
                    'add_new' => __('Add Group', 'portfolio'),
                    'add_new_item' => __('Add new Group ', 'portfolio'),
                    'edit' => __('Edit', 'portfolio'),
                    'edit_item' => __('Edit Group', 'portfolio'),
                    'new_item' => __('New Group', 'portfolio'),
                    'view' => __('View', 'portfolio'),
                    'view_item' => __('View Group', 'portfolio'),
                    'search_items' => __('Search Group', 'portfolio'),
                    'not_found' => __('Group not found', 'portfolio'),
                    'not_found_in_trash' => __('Group not found in trash', 'portfolio'),
                    'parent' => __('Parent Group', 'portfolio')
                ),
                'public' => true,
                'publicly_queryable' => false,
                'exclude_from_search' => true,
                'menu_position' => 18,
                'supports' => array('title','thumbnail','editor'),
                'taxonomies' => array(''),
                'menu_icon' => 'dashicons-admin-comments',
                'has_archive' => true
            )
        );
        
    }

    function add_meta_boxes(){
        add_meta_box( 'abv_services_id', 'Services', [(new ABVLiverillWidgets()), 'abv_services_meta_callback'], 'services' );
        add_meta_box( 'abv_group_id', 'Group', [(new ABVLiverillWidgets()), 'abv_group_meta_callback'], 'group' );
        add_meta_box( 'abv_post_id', 'Post', [(new ABVLiverillWidgets()), 'abv_post_meta_callback'], 'post' );
    }

    function save_post_data($post_id){
        if (
            get_post_type($post_id) == "services" or
            get_post_type($post_id) == "post" or
            get_post_type($post_id) == "group"

        ) {
            if (!isset($_POST['abv_nonceName']))
                return $post_id;
            // проверяем nonce нашей страницы, потому что save_post может быть вызван с другого места.
            if (!wp_verify_nonce($_POST['abv_nonceName'], $this->themeDir))
                return $post_id;

            // проверяем, если это автосохранение ничего не делаем с данными нашей формы.
            if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
                return $post_id;

            // проверяем разрешено ли пользователю указывать эти данные
            if ('page' == $_POST['post_type'] && !current_user_can('edit_page', $post_id)) {
                return $post_id;
            } elseif (!current_user_can('edit_post', $post_id)) {
                return $post_id;
            }
            //////////////////////////

            if (get_post_type($post_id) == "services"){
                if(isset($_POST['abv_services_position_field'])){
                    $services_position = sanitize_text_field( $_POST['abv_services_position_field'] );
                    update_post_meta($post_id, 'abv_services_position_meta_value_key', $services_position);
                } else {
                    update_post_meta($post_id, 'abv_services_position_meta_value_key', '');
                }

                if(isset($_POST['abv_services_class_field'])){
                    $services_class = sanitize_text_field( $_POST['abv_services_class_field'] );
                    update_post_meta($post_id, 'abv_services_class_meta_value_key', $services_class);
                } else {
                    update_post_meta($post_id, 'abv_services_class_meta_value_key', '');
                }
            }

            if (get_post_type($post_id) == "group"){
                if(isset($_POST['abv_group_position_field'])){
                    $group_position_field = sanitize_text_field( $_POST['abv_group_position_field'] );
                    update_post_meta($post_id, 'abv_group_position_meta_value_key', $group_position_field);
                } else {
                    update_post_meta($post_id, 'abv_group_position_meta_value_key', '');
                }

                if(isset($_POST['abv_group_type_field'])){
                    $group_type_field = sanitize_text_field( $_POST['abv_group_type_field'] );
                    update_post_meta($post_id, 'abv_group_type_meta_value_key', $group_type_field);
                } else {
                    update_post_meta($post_id, 'abv_group_type_meta_value_key', '');
                }

                if(isset($_POST['abv_group_age_field'])){
                    $group_age_field = sanitize_text_field( $_POST['abv_group_age_field'] );
                    update_post_meta($post_id, 'abv_group_age_meta_value_key', $group_age_field);
                } else {
                    update_post_meta($post_id, 'abv_group_age_meta_value_key', '');
                }

                if(isset($_POST['abv_group_color_field'])){
                    $group_color_field = sanitize_text_field( $_POST['abv_group_color_field'] );
                    update_post_meta($post_id, 'abv_group_color_meta_value_key', $group_color_field);
                } else {
                    update_post_meta($post_id, 'abv_group_color_meta_value_key', '');
                }
            }

            if (get_post_type($post_id) == "post"){
                if(isset($_POST['abv_post_position_field'])){
                    $post_position_field = sanitize_text_field( $_POST['abv_post_position_field'] );
                    update_post_meta($post_id, 'abv_post_position_meta_value_key', $post_position_field);
                } else {
                    update_post_meta($post_id, 'abv_post_position_meta_value_key', '');
                }

                if(isset($_POST['$post_first_field'])){
                    $post_first_field = sanitize_text_field( $_POST['$post_first_field'] );
                    update_post_meta($post_id, 'abv_post_first_meta_value_key', $post_first_field);
                } else {
                    update_post_meta($post_id, 'abv_post_first_meta_value_key', '');
                }
            }
        }
    }
}