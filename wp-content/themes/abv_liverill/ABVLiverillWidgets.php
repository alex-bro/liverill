<?php

class ABVLiverillWidgets
{
    function abv_services_meta_callback($portfolioForm){
        global $abv_liverill;
        wp_nonce_field($abv_liverill->themeDir, 'abv_nonceName');

        $services_position_field = esc_html(
            get_post_meta($portfolioForm->ID, 'abv_services_position_meta_value_key', true));

        $services_class_field = esc_html(
            get_post_meta($portfolioForm->ID, 'abv_services_class_meta_value_key', true));

        include('template-parts'.DIRECTORY_SEPARATOR.'liverill-services-metabox.php');
    }

    function abv_group_meta_callback($portfolioForm){
        global $abv_liverill;
        wp_nonce_field($abv_liverill->themeDir, 'abv_nonceName');

        $group_position_field = esc_html(
            get_post_meta($portfolioForm->ID, 'abv_group_position_meta_value_key', true));

        $group_type_field = esc_html(
            get_post_meta($portfolioForm->ID, 'abv_group_type_meta_value_key', true));

        $group_age_field = esc_html(
            get_post_meta($portfolioForm->ID, 'abv_group_age_meta_value_key', true));

        $group_color_field = esc_html(
            get_post_meta($portfolioForm->ID, 'abv_group_color_meta_value_key', true));

        include('template-parts'.DIRECTORY_SEPARATOR.'liverill-group-metabox.php');
    }

    function abv_post_meta_callback($portfolioForm){
        global $abv_liverill;
        wp_nonce_field($abv_liverill->themeDir, 'abv_nonceName');

        $post_position_field = esc_html(
            get_post_meta($portfolioForm->ID, 'abv_post_position_meta_value_key', true));

        $post_first_field = esc_html(
            get_post_meta($portfolioForm->ID, 'abv_post_first_meta_value_key', true));

        include('template-parts'.DIRECTORY_SEPARATOR.'liverill-post-metabox.php');
    }

    // показ 3х постов на главной
    static function show_post_on_main(){
        $args = array(
            'post_type'  => 'post',
            'posts_per_page' => 3,
            'category__in' => ['stati-rod','stati'],
        );
        $args['meta_query'] = array(
            array(
                'key' => 'abv_post_first_meta_value_key',
                'value' => '1'
            ),
            array(
                'key' => 'abv_post_position_meta_value_key',
            )
        );
        $args['meta_key'] = 'abv_post_position_meta_value_key';
        $args['orderby'] = 'meta_value_num';
        $args['order'] = 'ASC';

        $query = new WP_Query( $args );
        $post_found = $query->found_posts;
        $found = array();
        if ($post_found){
            foreach($query->posts as $item){
                $found[] = $item->ID;
                include('template-parts'.DIRECTORY_SEPARATOR.'liverill-post-main.php');
            }
        }
        wp_reset_postdata();

        if($post_found < 3){
            $args = array(
                'post_type'  => 'post',
                'posts_per_page' => 3 - $post_found,
                'orderby' => 'date',
                'order' => 'DESC',
                'post__not_in' => $found,
                'category_name' => 'stati'
            );
            $query = new WP_Query( $args );
            if ($query->found_posts){
                foreach($query->posts as $item){
                    include('template-parts'.DIRECTORY_SEPARATOR.'liverill-post-main.php');
                }
            }
        }
        wp_reset_postdata();
    }
}