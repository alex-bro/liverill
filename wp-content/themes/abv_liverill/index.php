<?php get_header('main'); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="container category_content">
				<?php
				if ( have_posts() ) {

					/* Start the Loop */
					while (have_posts()) : the_post();

						include('template-parts' . DIRECTORY_SEPARATOR . 'liverill-category.php');

					endwhile;

					if (function_exists('wp_pagenavi')) {
						wp_pagenavi();
					}


				}else {

					get_template_part('template-parts/content', 'none');

				};
				?>
			</div>


		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer('main');