<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>
<body>
<div class="wrapper_header">
	<header>
		<div class="container">
			<div class="row top_line">
				<div class="hidden-xs col-sm-3 col-md-3">
					<div class="galery_button">
						<?php echo wp_nav_menu(
							array(
								"theme_location" => "top_left",
							));
						?>
					</div>
				</div>
				<div class="col-xs-12 col-sm-9 col-md-6">
					<div class="logo">
						<a href="/">
							<?php echo get_the_post_thumbnail(ABVFunctions::get_id_by_slug('main'),'full'); ?>
						</a>
					</div>
				</div>
				<div class="hidden-xs hidden-sm col-md-3">
					<?php get_search_form(); ?>
				</div>
			</div>
			<div class="row">
				<div class="name_school">
					<h1><?php echo get_bloginfo('description') ?></h1>
				</div>
			</div>
		</div>
		<!--////////////////////////////////////////// -->
		<div class="container">
			<nav class="navbar navbar-default">

				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>

				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<?php echo wp_nav_menu(
						array(
							"theme_location" => "top",
							'items_wrap'=> '<ul id="%1$s" class="%2$s nav navbar-nav">%3$s</ul>'
						));
					?>

				</div><!-- /.navbar-collapse -->
				<!-- /.container-fluid -->
		</div>

		</nav>
	</header>