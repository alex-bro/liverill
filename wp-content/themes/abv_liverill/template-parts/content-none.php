<section class="no-results not-found">
	<header class="page-header">
		<h1 class="page-title"><?php esc_html_e( 'Nothing Found', 'abv_liverill' ); ?></h1>
	</header><!-- .page-header -->

</section><!-- .no-results -->
