<div class="col-md-4">
    <a href="<?php echo get_post_permalink($item->ID) ?>">
        <div class="article">
            <div class="img_wrap">
                <?php
                $thumb = get_the_post_thumbnail($item->ID, 'full');
                if (!$thumb) {echo '<img src="'.get_stylesheet_directory_uri().'/img/photo_post.png'.'" alt="">';}
                else {echo $thumb;}
                ?>
            </div>
            <div class="wrap">
                <div class="title">
                    <?php echo $item->post_title ?>
                </div>
                <div class="data">
                    <img src="<?php echo bloginfo('template_directory'); ?>/img/clock.png" alt="">
                    <?php echo mb_convert_case(get_post_time( 'j F Y, H:s', false, $item->ID, true), MB_CASE_TITLE, "UTF-8"); ?>
                </div>
                <div class="views">
                    <img src="<?php echo bloginfo('template_directory'); ?>/img/eye.png" alt="">
                    <?php echo get_post_meta( $item->ID, 'abv_post_views',true ) ?>
                </div>
            </div>
            <div class="text">
                  <?php echo mb_substr(strip_tags($item->post_content),0,112).'...' ?>
            </div>
        </div>
    </a>
</div>