<div class="row">
    <div class="col-md-12 category_content_item">
        <?php
            $thumb = get_the_post_thumbnail($post->ID, 'thumbnail');
            if (!$thumb) {echo '<img src="'.get_stylesheet_directory_uri().'/img/photo_post.png'.'" alt="">';}
            else {echo $thumb;}
        ?>
        <a href="<?php the_permalink() ?>"><h3><?php the_title()  ?></h3></a>
        <div class="data">
            <img src="<?php echo bloginfo('template_directory'); ?>/img/clock.png" alt="">
            <?php echo mb_convert_case(get_post_time( 'j F Y, H:s', false, $post->ID, true), MB_CASE_TITLE, "UTF-8"); ?>
        </div>
        <div class="views">
            <img src="<?php echo bloginfo('template_directory'); ?>/img/eye.png" alt="">
            <?php echo get_post_meta( $post->ID, 'abv_post_views',true ) ?>
        </div>
        <br>
        <div class="text">
            <?php the_content('<p class="view_more">Подробнее </p>') ?>
        </div>
    </div>
</div>