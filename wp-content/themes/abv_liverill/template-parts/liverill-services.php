<div class="col15-md-3">
    <div class="item">
        <?php echo get_the_post_thumbnail($item->ID,'full',array('class'=>get_post_meta( $item->ID, 'abv_services_class_meta_value_key',true ))); ?>
        <div class="title">
            <?php echo $item->post_title ?>
        </div>
        <div class="text">
            <?php echo ABVFunctions::get_content_by_id($item->ID) ?>
        </div>
    </div>
</div>
