<?php if (!defined('ABSPATH')) exit; ?>
<table>
    <tr>
        <td><label for="abv_group_position_field"><?php echo __("Position","abv-portfolio") ?></td>
        <td style="width: 100%;">
            <input type="text" id="abv_group_position_field" name="abv_group_position_field"
                   value="<?php echo $group_position_field; ?>" style="width: 100%;" >
        </td>
    </tr>
    <tr>
        <td><label for="abv_group_type_field"><?php echo __("Type","abv-portfolio") ?></td>
        <td style="width: 100%;">
            <input type="text" id="abv_group_type_field" name="abv_group_type_field"
                   value="<?php echo $group_type_field; ?>" style="width: 100%;" >
        </td>
    </tr>
    <tr>
        <td><label for="abv_group_age_field"><?php echo __("Age","abv-portfolio") ?></td>
        <td style="width: 100%;">
            <input type="text" id="abv_group_age_field" name="abv_group_age_field"
                   value="<?php echo $group_age_field; ?>" style="width: 100%;" >
        </td>
    </tr>
    <tr>
        <td><label for="abv_group_color_field"><?php echo __("Color","abv-portfolio") ?></td>
        <td style="width: 100%;">
            <input type="text" id="abv_group_color_field" name="abv_group_color_field"
                   value="<?php echo $group_color_field; ?>" style="width: 100%;" >
        </td>
    </tr>
</table>