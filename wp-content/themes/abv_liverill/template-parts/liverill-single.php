<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <div class="container">
            <div class="row">
                <div class="col-md-12 single_content">
                    <h1><?php the_title()  ?></h1>
                    <div class="data">
                        <?php echo mb_convert_case(get_post_time( 'j F Y, H:s', false, $post->ID, true), MB_CASE_TITLE, "UTF-8"); ?>
                    </div>
                    <?php echo get_the_post_thumbnail($post->ID,'full'); ?>
                    <div class="text">
                        <?php the_content() ?>
                    </div>
                </div>
            </div>
        </div>
    </main><!-- #main -->
</div><!-- #primary -->