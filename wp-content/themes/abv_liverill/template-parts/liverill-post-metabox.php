<?php if (!defined('ABSPATH')) exit; ?>
<table>
    <tr>
        <td><label for="abv_post_position_field"><?php echo __("Position","abv-portfolio") ?></td>
        <td style="width: 100%;">
            <input type="text" id="abv_post_position_field" name="abv_post_position_field"
                   value="<?php echo $post_position_field; ?>" style="width: 100%;" >
        </td>
    </tr>
    <tr>
        <td><label for="$post_first_field"><?php echo __("On main","abv-portfolio") ?></td>
        <td style="width: 100%;">
            <input type="hidden" id="$post_first_field" name="$post_first_field"
                   value="">
            <input type="checkbox" id="$post_first_field" name="$post_first_field"
                   value="1" <?php checked( $post_first_field, '1' ); ?> style="">
        </td>
    </tr>
</table>