<div class="col-md-3">
    <div class="item">
        <?php echo get_the_post_thumbnail($item->ID,'full'); ?>
        <div class="type" style="color: <?php echo get_post_meta( $item->ID, 'abv_group_color_meta_value_key',true ) ?>">
            <?php echo get_post_meta( $item->ID, 'abv_group_type_meta_value_key',true ) ?>
        </div>
        <div class="name" style="color: <?php echo get_post_meta( $item->ID, 'abv_group_color_meta_value_key',true ) ?>">
            <?php echo $item->post_title ?>
        </div>
        <div class="age">
            <?php echo get_post_meta( $item->ID, 'abv_group_age_meta_value_key',true ) ?>
        </div>
    </div>
</div>
