<?php if (!defined('ABSPATH')) exit; ?>
<table>
    <tr>
        <td><label for="abv_services_position_field"><?php echo __("Position","abv-portfolio") ?></td>
        <td style="width: 100%;">
            <input type="text" id="abv_services_position_field" name="abv_services_position_field"
                   value="<?php echo $services_position_field; ?>" style="width: 100%;" >
        </td>
    </tr>
    <tr>
        <td><label for="abv_services_class_field"><?php echo __("Class","abv-portfolio") ?></td>
        <td style="width: 100%;">
            <input type="text" id="abv_services_class_field" name="abv_services_class_field"
                   value="<?php echo $services_class_field; ?>" style="width: 100%;" >
        </td>
    </tr>
</table>