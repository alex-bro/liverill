
		<div class="container">
			<div class="row">
				<?php echo do_shortcode('[abv_gallery id="51" class="owl-carousel"]') ?>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="welcome">

		<div class="row">
			<div class="col-xs-12">
				<div class="title">
					<?php echo ABVFunctions::get_title_by_slug('dobro-pozhalovat') ?>
				</div>
				<div class="text">
					<?php echo ABVFunctions::get_content_by_slug('dobro-pozhalovat') ?>
				</div>
			</div>
			<div class="row">
				<?php ABVFunctions::get_all_post_type('services','abv_services_position_meta_value_key','liverill-services.php') ?>
			</div>
		</div>
	</div>
	<hr>
	<div class="groups">
		<div class="row">
			<div class="col-xs-12">
				<div class="title">
					<?php echo ABVFunctions::get_title_by_slug('nashi-gruppy') ?>
				</div>
				<div class="text">
					<?php echo ABVFunctions::get_content_by_slug('nashi-gruppy') ?>
				</div>
			</div>
		</div>
		<div class="row">
			<?php ABVFunctions::get_all_post_type('group','abv_group_position_meta_value_key','liverill-groupe.php') ?>
		</div>
	</div>
	<div class="attension">
		<div class="row">
			<div class="col-md-8 bg_gray">
				<div class="title">
					<?php echo ABVFunctions::get_title_by_slug('ne-sleduet-odnako-zabyvat') ?>
				</div>
				<div class="text">
					<?php echo ABVFunctions::get_content_by_slug('ne-sleduet-odnako-zabyvat') ?>
				</div>
			</div>
			<div class="col-md-4">
				<?php echo get_the_post_thumbnail(ABVFunctions::get_id_by_slug('ne-sleduet-odnako-zabyvat'),'full'); ?>
			</div>
		</div>
	</div>
	<div class="photo">
		<div class="title">
			<?php echo ABVFunctions::get_title_by_slug('fotogalereya', 'gallery') ?>
		</div>
		<div class="row">
			<?php echo do_shortcode('[abv_gallery id="116" class="slider_photo" itemClass="item"]') ?>
			<div class="see_all"><a href="#">смотреть все</a></div>
			<hr>
		</div>
	</div>
	<div class="new_articls">
		<div class="row">
			<div class="col-xs-12">
				<div class="title">
					новые статьи
				</div>
			</div>
		</div>
		<div class="row">
			<?php ABVLiverillWidgets::show_post_on_main() ?>
		</div>

		<div class="row">
		  <div class="show_all_articls">
			<a href="#">все статьи</a>
		  </div>
		</div>

</div>
</div>
</div>
