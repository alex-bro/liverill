<?php
get_header('main'); ?>
	<?php
	while ( have_posts() ) : the_post();

		include('template-parts'.DIRECTORY_SEPARATOR.'liverill-main.php');

	endwhile; // End of the loop.
	?>
<?php
get_footer('main');
