<?php wp_footer(); ?>
<div class="footer">
	<div class="container">
		<div class="row wrapp">
			<div class="col15-md-5">
				<div class="logo">
					<img src="<?php echo bloginfo('template_directory'); ?>/images/logo_footer.png" alt="">
					<div class="copy">
						&copy;  2016 «Живой родник». Детская воскресная школа при Свято-Духовском кафедральном соборе.
					</div>
					<div class="siteprosto">
						Все права защищены. Разработка сайта <span>SiteProsto</span>
					</div>
				</div>
			</div>
			<div class="col15-md-2">
				<div class="about">
					<div class="title">
						о школе
					</div>
					<ul>
						<li><a href="#">наша жизнь</a></li>
						<li><a href="#">выпускники</a></li>
						<li><a href="#">выпускники</a></li>
						<li><a href="#">что изучаем</a></li>
					</ul>
				</div>
			</div>
			<div class="col15-md-2">
				<div class="footer_menu">
					<ul>
						<li><a href="#">родителям</a></li>
						<li><a href="#">фотогалерея</a></li>
						<li><a href="#">видеогалерея</a></li>
						<li><a href="#">статьи</a></li>
						<li><a href="#">контактЫ</a></li>
					</ul>
				</div>
			</div>
			<div class="col15-md-3">
				<div class="subscription">
					<div class="title">
						подписка на новости
					</div>
					<div class="want">
						Хочешь быть в курсе событий!?
					</div>
					<form action="">
						<input type="text" placeholder="E-mail">
						<button><img src="<?php echo bloginfo('template_directory'); ?>/images/footer_plane.png" alt=""></button>
					</form>
				</div>
			</div>
			<div class="col15-md-3">
				<div class="contacts">
					<ul>

						<li><a href="#"><img src="<?php echo bloginfo('template_directory'); ?>/images/cont_locate.png" alt="">г. Херсон, ул. Декабристов, 36</a></li>
						<li><a href="#"><img src="<?php echo bloginfo('template_directory'); ?>/images/cont_phone.png" alt="">095 844 62 60</a></li>
						<li><a href="#"><img src="<?php echo bloginfo('template_directory'); ?>/images/cont_email.png" alt="">info@namecompany.com</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
</body>
</html>
