<?php

if ( ! defined( 'ABV_THEME_URL' ) )
    define( 'ABV_THEME_URL', get_template_directory_uri() );
if ( ! defined( 'ABV_THEME_PATH' ) )
    define( 'ABV_THEME_PATH', get_template_directory() );

include_once('abv_theme_init.php');
include_once('abv_admin.php');
include_once('abv_functions.php');
include_once('abv_widgets.php');
include_once('abv_options.php');
include_once('abv_options_remove.php');

add_action('wp_enqueue_scripts', 'abv_load_style_script');
add_action('wp_ajax_abv_ajax', 'abv_action_ajax_callback');
add_action('wp_ajax_nopriv_abv_ajax', 'abv_action_ajax_callback');
add_action( 'admin_enqueue_scripts', 'abv_include_image_js' );

// подключение інтпута вибора картинки
function abv_include_image_js() {
    if ( ! did_action( 'wp_enqueue_media' ) ) {
        wp_enqueue_media();
    }
    wp_enqueue_script( 'abv_image_admin', ABV_THEME_URL . '/js/abv_image_admin.js', array('jquery'), null, false );
    wp_enqueue_style('abv_admin', ABV_THEME_URL . '/css/admin.css');
}


function abv_load_style_script(){
    wp_register_script('abv_jquery', ABV_THEME_URL . "/js/jquery-1.11.1.min.js",false,false);
    wp_register_script('bootstrap-min', ABV_THEME_URL . "/js/bootstrap.min.js",false,false,true);
    wp_register_script('owl-carousel-min', ABV_THEME_URL . "/js/owl.carousel.min.js",false,false,true);
    wp_register_script('abv_common', ABV_THEME_URL . "/js/common.js",false,false,true);


    wp_enqueue_script('abv_jquery');
    wp_enqueue_script('bootstrap-min');
    wp_enqueue_script('owl-carousel-min');
    wp_enqueue_script('abv_common');

    wp_localize_script('abv_common', 'abv_ajax',
        array(
            'url' => admin_url('admin-ajax.php'),
            'nonce' => wp_create_nonce('abv_ajax_nonce'),
        )
    );


    wp_enqueue_style('abv_fonts', ABV_THEME_URL . '/css/fonts.css');
    wp_enqueue_style('abv_styles', ABV_THEME_URL . '/css/styles.css');
    wp_enqueue_style('bootstrap_col_15', ABV_THEME_URL . '/css/bootstrap_col_15.css');
    wp_enqueue_style('owl_carousel', ABV_THEME_URL . '/css/owl.carousel.css');
    wp_enqueue_style('font-awesome', ABV_THEME_URL . '/fonts/font-awesome-4.6.1/css/font-awesome.min.css');
    wp_enqueue_style('abv_style', ABV_THEME_URL . '/css/style.css');

}

function abv_action_ajax_callback(){
    $product_id = intval( $_POST['product_id'] );
    $nonce = $_POST['nonce'];

    if ( !wp_verify_nonce($nonce, 'abv_ajax_nonce' )) wp_die ( 'Stop!');

    //echo $product_id;
    if($product_id) abv_widget_popup($product_id);

    wp_die();
}



