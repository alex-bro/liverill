<?php

//add_action( 'admin_menu', 'abv_remove_menus' ); // отчистка админки от страниц управления
//add_filter('admin_footer_text', 'abv_remove_footer_admin'); // отчистка футера админки
//add_filter( 'update_footer', 'abv_change_footer_version', 9999); // удаление версии в админке
show_admin_bar(false);/* Отключаем админ панель для всех пользователей. */
//add_action( 'wp_before_admin_bar_render', 'abv_admin_bar',999 ); //отчистка верхнего меню в админке

// отчистка админки от страниц управления
function abv_remove_menus(){
    //remove_menu_page( 'index.php' );                  //Консоль
    //remove_menu_page( 'edit.php' );                   //Записи
    //remove_menu_page( 'upload.php' );                 //Медиафайлы
    //remove_menu_page( 'edit.php?post_type=page' );    //Страницы
    remove_menu_page( 'edit-comments.php' );          //Комментарии
    //remove_menu_page( 'themes.php' );                 //Внешний вид
    //remove_menu_page( 'plugins.php' );                //Плагины
    //remove_menu_page( 'users.php' );                  //Пользователи
    remove_menu_page( 'tools.php' );                  //Инструменты
    //remove_menu_page( 'options-general.php' );        //Настройки
}
/*свое имя в футере админки*/
function abv_remove_footer_admin () {
    echo "Здесь ваш текст, либо ссылка";
}
// удаление версии в админке
function abv_change_footer_version() {return ' ';}
//отчистка верхнего меню в админке
function abv_admin_bar() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('wp-logo');
    $wp_admin_bar->remove_menu('about');
    $wp_admin_bar->remove_menu('wporg');
    $wp_admin_bar->remove_menu('documentation');
    $wp_admin_bar->remove_menu('support-forums');
    $wp_admin_bar->remove_menu('feedback');
    $wp_admin_bar->remove_menu('add');
    $wp_admin_bar->remove_menu('view-site');
    $wp_admin_bar->remove_menu('site-name');
    $wp_admin_bar->remove_menu('new-content');
    $wp_admin_bar->remove_menu('comments');
    $wp_admin_bar->remove_menu('my-account');
}
