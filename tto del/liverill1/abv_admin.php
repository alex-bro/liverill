<?php

add_action('init','abv_create_post_type');
add_action('add_meta_boxes', 'abv_add_meta_boxes');// зарегистрировать метаблок для любого типа записи
add_action( 'save_post', 'abv_save_post_data' );//срабатывающее всякий раз, когда запись

// создаем типі
function abv_create_post_type (){
    // складові
    register_post_type('ingredients',
        array(
            'labels' => array(
                'name' => 'Складові',
                'singular_name' => 'Складова',
                'add_new' => 'Додати нову',
                'add_new_item' => 'Додати нову складову',
                'edit' => 'Редагувати',
                'edit_item' => 'Редагувати складову',
                'new_item' => 'Нова складова',
                'view' => 'Дивитись',
                'view_item' => 'Дивитись складову',
                'search_items' => 'Шукати складову',
                'not_found' => 'Складову не знайдено',
                'not_found_in_trash' => 'У кошику складову не знайдено',
                'parent' => 'Предок складової'
            ),
            'public' => true,
            'menu_position' => 15,
            'supports' => array('title', 'thumbnail'),
            'taxonomies' => array(''),
            'menu_icon' => ABV_THEME_URL.'/images/ingredient.png',
            'has_archive' => true
        )
    );
// відгуки
    register_post_type('reviews',
        array(
            'labels' => array(
                'name' => 'Відгуки',
                'singular_name' => 'Відгук',
                'add_new' => 'Додати новий',
                'add_new_item' => 'Додати новий відгук',
                'edit' => 'Редагувати',
                'edit_item' => 'Редагувати відгук',
                'new_item' => 'Новий відгук',
                'view' => 'Дивитись',
                'view_item' => 'Дивитись відгук',
                'search_items' => 'Шукати відгук',
                'not_found' => 'Відгук не знайдено',
                'not_found_in_trash' => 'У кошику відгука не знайдено',
                'parent' => 'Предок відгука'
            ),
            'public' => true,
            'menu_position' => 16,
            'supports' => array('title', 'thumbnail', 'editor'),
            'taxonomies' => array(''),
            'menu_icon' => ABV_THEME_URL.'/images/review.png',
            'has_archive' => true
        )
    );
// продукті
    register_post_type('products',
        array(
            'labels' => array(
                'name' => 'Продукти',
                'singular_name' => 'Продукт',
                'add_new' => 'Додати новий',
                'add_new_item' => 'Додати новий продукт',
                'edit' => 'Редагувати',
                'edit_item' => 'Редагувати продукт',
                'new_item' => 'Новий продукт',
                'view' => 'Дивитись',
                'view_item' => 'Дивитись продукт',
                'search_items' => 'Шукати продукт',
                'not_found' => 'Продукт не знайдено',
                'not_found_in_trash' => 'У кошику продукта не знайдено',
                'parent' => 'Предок продукта'
            ),
            'public' => true,
            'menu_position' => 17,
            'supports' => array('title', 'thumbnail', 'editor'),
            'taxonomies' => array(''),
            'menu_icon' => ABV_THEME_URL.'/images/products.png',
            'has_archive' => true
        )
    );

    // определяем заголовки для 'категорія'
    $labels = array(
        'name' => _x( 'Категорії', 'taxonomy general name' ),
        'singular_name' => _x( 'Категорія', 'taxonomy singular name' ),
        'search_items' =>  __( 'Шукати категорію' ),
        'all_items' => __( 'Усі категорії' ),
        'parent_item' => __( 'Предок категорій' ),
        'parent_item_colon' => __( 'Предок категорії:' ),
        'edit_item' => __( 'Редагувати категорію' ),
        'update_item' => __( 'Оновити категорію' ),
        'add_new_item' => __( 'Додати нову категорію' ),
        'new_item_name' => __( 'Ім\'я нової категорії' ),
        'menu_name' => __( 'Категорія' ),
    );

    // Добавляем древовидную таксономию 'категория' (как категории)
    register_taxonomy('products_cat', array('products'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'products_cat' ),
    ));
// акції
    register_post_type('sales',
        array(
            'labels' => array(
                'name' => 'Акції',
                'singular_name' => 'Акція',
                'add_new' => 'Додати акцію',
                'add_new_item' => 'Додати нову акцію',
                'edit' => 'Редагувати',
                'edit_item' => 'Редагувати акцію',
                'new_item' => 'Нова акція',
                'view' => 'Дивитись',
                'view_item' => 'Дивитись акцію',
                'search_items' => 'Шукати акцію',
                'not_found' => 'Акцію не знайдено',
                'not_found_in_trash' => 'У кошику акцію не знайдено',
                'parent' => 'Предок акції'
            ),
            'public' => true,
            'menu_position' => 18,
            'supports' => array('title', 'thumbnail', 'editor'),
            'taxonomies' => array(''),
            'menu_icon' => ABV_THEME_URL.'/images/sales.jpg',
            'has_archive' => true
        )
    );

    if ( function_exists( 'add_image_size' ) ) {
        add_image_size( 'product-thumb', 380, 380, false);
        add_image_size( 'sales-thumb', 480, 480, false);
    }

}

// создаем для мета
function abv_add_meta_boxes(){
    add_meta_box( 'abv_products_id', 'Продукція', 'abv_products_meta_callback', 'products' );
    add_meta_box( 'abv_sales_id', 'Акції', 'abv_sales_meta_callback', 'sales' );
}

// рисуем инпуті продукту
function abv_products_meta_callback($productsForm){
    wp_nonce_field( ABV_THEME_PATH.'/'.__FILE__, 'abv_nonceName' );

    $products_subtitle_field = esc_html(
        get_post_meta($productsForm->ID, 'products_subtitle_meta_value_key', true));
    $products_price_field = esc_html(
        get_post_meta($productsForm->ID, 'products_price_meta_value_key', true));


    ?>
    <table>
        <tr>
            <td><label for="products_subtitle_field">Описание</td>
            <td style="width: 100%;"><input type="text" id="products_subtitle_field" name="products_subtitle_field"
                       value="<?php echo $products_subtitle_field; ?>" size="20" style="width: 100%;" >
            </td>
        </tr>
        <tr>
            <td><label for="products_price_field">Цена</td>
            <td><input type="text" id="products_price_field" name="products_price_field" 
                       value="<?php echo $products_price_field; ?>"
                       size="20" pattern="\-?\d+(\.\d{0,2})?" required>
            </td>
        </tr>
    </table>

    <?php
    abv_ingredient($productsForm->ID);
}

// записіваем инпуті мета
function abv_save_post_data($post_id){

    if (!isset($_POST['abv_nonceName']))
        return $post_id;

    // проверяем nonce нашей страницы, потому что save_post может быть вызван с другого места.
    if ( ! wp_verify_nonce( $_POST['abv_nonceName'], ABV_THEME_PATH.'/'.__FILE__ ) )
        return $post_id;

    // проверяем, если это автосохранение ничего не делаем с данными нашей формы.
    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
        return $post_id;

    // проверяем разрешено ли пользователю указывать эти данные
    if ( 'page' == $_POST['post_type'] && ! current_user_can( 'edit_page', $post_id ) ) {
        return $post_id;
    } elseif( ! current_user_can( 'edit_post', $post_id ) ) {
        return $post_id;
    }

    /////////////блок продуктов/////////////////

    // Очищаем значение поля input.
    $subtitle = sanitize_text_field( $_POST['products_subtitle_field'] );
    $price = sanitize_text_field( $_POST['products_price_field'] );

    $string_id_ingredient = '';
    foreach ($_POST as $key => $item) {
        if (strpos($key, 'ingredients') !== false and $item !== '' and is_array($item) == false) $string_id_ingredient .= $item.',';
    }
    if(isset($_POST['abv_add_ingredients'])){
        foreach ($_POST['abv_add_ingredients'] as $item) {
            $arr = explode('-6tJdfr8UR-',$item);
            $string_id_ingredient .= $arr[0].',';
        }
    }
    $string_id_ingredient = substr($string_id_ingredient, 0, -1);




    // запись
    if ($subtitle) update_post_meta( $post_id, 'products_subtitle_meta_value_key', $subtitle );
    if ($price) update_post_meta( $post_id, 'products_price_meta_value_key', $price );
    if ($string_id_ingredient) update_post_meta( $post_id, 'abv_ingredients_value_key', $string_id_ingredient );

    /////////////блок акций/////////////////

    $_sales_price = sanitize_text_field( $_POST['sales_price_field'] );
    // запись
    if ($_sales_price) update_post_meta( $post_id, 'sales_price_meta_value_key', $_sales_price );
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
// вібираем складові для продукту
function abv_ingredient($id){
    // читаємо строку з мета продкутц айди складових
    $ingredient_short_field = esc_html(get_post_meta($id, 'abv_ingredients_value_key', true));
    // бкркм усі інгрідієнти у асоц масиві
    $all_stores = abv_get_id_title('ingredients');
    // якщо вони є
    if($ingredient_short_field){
        // розбираємо строку у масив
        $ingredient_short_field = explode(',', $ingredient_short_field);
        // додаємо до масива назви
        $load_id_name = abv_add_name($ingredient_short_field, 'ingredients');
    }

    echo '<div class="ingredient_wrap">';
    echo "<h3 style='text-align: center'>Блок інгридієнтів</h3>";
    if (isset($load_id_name)) {
        echo '<p>Щоб <span>видалити</span> інгридієнт зніміть з нього галочку... </p>';
        foreach ($load_id_name as $key => $val){
            ?>
            <label class="selectit" for="ingredients<?php echo $key; ?>">
                <input type="checkbox" name="ingredients<?php echo $key; ?>" class="ingredient" value="<?php echo $key; ?>" checked>
                <?php echo $val; ?>
            </label>
            <?php
        }
    }

    ?>
    <br>
    <p>Щоб додати інгридієнт або інгридієнти оберіть їх у списку. За допомогою клавіши "Ctrl" можно обрати декілька інгридієнтів.</p>
    <br>
    <select multiple name="abv_add_ingredients[]" size="10">
        <?php
        foreach ($all_stores as $key => $val){
            if(isset($load_id_name)){
                if (array_search($val,$load_id_name) === false){
                    echo "<option value='".$key."-6tJdfr8UR-".$val."' >".$val;
                }
            } else echo "<option value='".$key."-6tJdfr8UR-".$val."' >".$val;
        }
        ?>
    </select>
    <?php

    echo '</div> <hr>';
}

// рисуем инпуті акції
function abv_sales_meta_callback($productsForm){
    wp_nonce_field( ABV_THEME_PATH.'/'.__FILE__, 'abv_nonceName' );

    $sales_price_field = esc_html(
        get_post_meta($productsForm->ID, 'sales_price_meta_value_key', true));
    ?>
    <table>
        <tr>
            <td><label for="sales_price_field">Цена</td>
            <td><input type="text" id="sales_price_field" name="sales_price_field"
                       value="<?php echo $sales_price_field; ?>"
                       size="20" pattern="\-?\d+(\.\d{0,2})?" required>
            </td>
        </tr>
    </table>

    <?php
}

