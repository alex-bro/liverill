<?php
/*
Template Name: Index
*/
?>
<?php get_header(); ?>
	<div class="wrapper_header">
		<header>
			<div class="container">
				<div class="row top_line">
					<div class="hidden-xs col-sm-3 col-md-3">
						<div class="galery_button">
							<a href="#"><img src="<?php echo ABV_THEME_URL; ?>/images/photo_galery.png" alt=""></a>
							<a href="#"><img src="<?php echo ABV_THEME_URL; ?>/images/videogalery.png" alt=""></a>
							<a href="#"><img src="<?php echo ABV_THEME_URL; ?>/images/posts.png" alt=""></a>
						</div> 
					</div>
					<div class="col-xs-12 col-sm-9 col-md-6">
						<div class="logo">
							<a href="/">
								<img src="<?php echo ABV_THEME_URL; ?>/images/logo.png" alt="">
							</a>
						</div>
					</div>
					<div class="hidden-xs hidden-sm col-md-3">
						<form action="" method="post" class="text-right">
							<input type="text" placeholder="Поиск">
							<button type="submit">
								<img src="<?php echo ABV_THEME_URL; ?>/images/serch.png" alt="">
							</button>
						</form>
					</div>
				</div>
				<div class="row">
					<div class="name_school">
						<h1>Детская воскресная школа при <br> Свято-Духовском кафедральном соборе</h1>
					</div>
				</div>
			</div>
			<!--////////////////////////////////////////// -->
			<div class="container">
				<nav class="navbar navbar-default">

					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav">
							<li>
								<a href="#" id="about">
									<img src="<?php echo ABV_THEME_URL; ?>/images/b_about.png" alt="">
									о школе
								</a>
							</li>
							<li>
								<a href="#" id="life">
									<img src="<?php echo ABV_THEME_URL; ?>/images/b_flover.png" alt="">
									наша жизнь
								</a>
							</li>
							<li>
								<a href="#" id="magistr">
									<img src="<?php echo ABV_THEME_URL; ?>/images/b_magistr.png" alt="">
									выпускники
								</a>
							</li>
							<li>
								<a href="#" id="docs">
									<img src="<?php echo ABV_THEME_URL; ?>/images/b_scroll.png" alt="">
									документы
								</a>
							</li>
							
							<li>
								<a href="#" id="book">
									<img src="<?php echo ABV_THEME_URL; ?>/images/b_book.png" alt="">
									что изучаем
								</a>
							</li>
							<li>
								<a href="#" id="parents">
									<img src="<?php echo ABV_THEME_URL; ?>/images/b_people.png" alt="">
									родителям
								</a>
							</li>
							<li>
								<a href="#" id="mail" >
									<img src="<?php echo ABV_THEME_URL; ?>/images/b_people.png" alt="">
									контакты
								</a>
							</li>
						</ul>


					</div><!-- /.navbar-collapse -->
					<!-- /.container-fluid -->
				</div>

			</nav>
		</header>
		<div class="container">
			<div class="row">
				<div class="owl-carousel">
					<div class="owl-item"><img src="<?php echo ABV_THEME_URL; ?>/images/slider.jpg" alt=""></div>
					<div class="owl-item"><img src="<?php echo ABV_THEME_URL; ?>/images/slider.jpg" alt=""></div>
					<div class="owl-item"><img src="<?php echo ABV_THEME_URL; ?>/images/slider.jpg" alt=""></div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="welcome">

		<div class="row">
			<div class="col-xs-12">
				<div class="title">
					Добро пожаловать!
				</div>
				<div class="text">
					<p> Наша Воскресная школа была организована в 1999г. более 15 лет назад по благословению настоятеля нашего храма протоирея Михаила Васильева. С каждым днем, желающих узнать о Боге, прикоснуться к многовековой истории нашей православной культуры становится все больше и больше. Сейчас в школе занимаются более 80 детей от 4 до 15 лет. В соответствии с этим существует 6 возрастных групп. Двери нашей школы открыты для каждого, чье сердце горит желанием идти навстречу Богу. Обучение в Воскресной школе бесплатное.</p>
					<p>Все занятия интегрированные. Наряду с изучением Закона Божьего, Истории Русской Православной Церкви, Основ православного вероучения, Евангельской истории, дети рисуют, вышивают, шьют, лепят и даже готовят. Через различные виды прикладного искусства для детей раскрывается богатство нашей культуры. Огромное количество творческих мастерских, мастер-классов, экскурсий и праздников делают занятия в Воскресной школе неповторимыми и увлекательными.</p>
				</div>
			</div>
			<div class="row">
				<div class="col15-md-3">
					<div class="item">
						<img class="globus" src="<?php echo ABV_THEME_URL; ?>/images/item_globus.png" alt="">
						<div class="title">
							название пункта
						</div>
						<div class="text">
							Огромное количество творческих мастерских, мастер-классов
						</div>
					</div>
				</div>
				<div class="col15-md-3">
					<div class="item">
						<img class="biblie" src="<?php echo ABV_THEME_URL; ?>/images/item_biblie.png" alt="">
						<div class="title">
							название пункта
						</div>
						<div class="text">
							Огромное количество творческих мастерских, мастер-классов
						</div>
					</div>
				</div>
				<div class="col15-md-3">
					<div class="item">
						<img class="pallete" src="<?php echo ABV_THEME_URL; ?>/images/item_pallete.png" alt="">
						<div class="title">
							название пункта
						</div>
						<div class="text">
							Огромное количество творческих мастерских, мастер-классов
						</div>
					</div>
				</div>
				<div class="col15-md-3">
					<div class="item">
						<img class="cherry" src="<?php echo ABV_THEME_URL; ?>/images/item_cherry.png" alt="">
						<div class="title">
							название пункта
						</div>
						<div class="text">
							Огромное количество творческих мастерских, мастер-классов
						</div>
					</div>
				</div>
				<div class="col15-md-3">
					<div class="item">
						<img class="bus" src="<?php echo ABV_THEME_URL; ?>/images/item_bus.png" alt="">
						<div class="title">
							название пункта
						</div>
						<div class="text">
							Огромное количество творческих мастерских, мастер-классов
						</div>
					</div>
				</div>
			</div>


		</div>
	</div>
	<hr>
	<div class="groups">
		<div class="row">
			<div class="col-xs-12">
				<div class="title">
					наши группы
				</div>
				<div class="text">
					Не следует, однако забывать, что укрепление и развитие структуры влечет за собой процесс внедрения и модернизации модели развития. Не следует, однако забывать, что консультация с широким активом способствует подготовки и реализации системы обучения кадров, соответствует насущным потребностям. Таким образом реализация намеченных плановых заданий представляет собой интересный эксперимент проверки системы обучения
				</div>
			</div>	
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="item">
					<img src="<?php echo ABV_THEME_URL; ?>/images/group1.png" alt="">
					<div class="type groupe1">
						Младшая группа 
					</div>
					<div class="name groupe1">
						«Солнышко»
					</div>
					<div class="age">
						дети от 4 до 6 лет
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="item">
					<img src="<?php echo ABV_THEME_URL; ?>/images/group1.png" alt="">
					<div class="type groupe2">
						Младшая группа 
					</div>
					<div class="name groupe2">
						«Солнышко»
					</div>
					<div class="age">
						дети от 4 до 6 лет
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="item">
					<img src="<?php echo ABV_THEME_URL; ?>/images/group1.png" alt="">
					<div class="type groupe3">
						Младшая группа 
					</div>
					<div class="name  groupe3">
						«Солнышко»
					</div>
					<div class="age">
						дети от 4 до 6 лет
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="item">
					<img src="<?php echo ABV_THEME_URL; ?>/images/group1.png" alt="">
					<div class="type  groupe4">
						Младшая группа 
					</div>
					<div class="name  groupe4">
						«Солнышко»
					</div>
					<div class="age">
						дети от 4 до 6 лет
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="attension">
		<div class="row">
			<div class="col-md-8 bg_gray">
				<div class="title">
					Не следует, однако забывать
				</div>
				<div class="text">
					Что укрепление и развитие структуры влечет за собой процесс внедрения и модернизации модели развития. Не следует, однако забывать, что консультация с широким активом способствует подготовки и реализации системы обучения кадров, соответствует насущным потребностям. Таким образом реализация намеченных плановых заданий представляет собой интересный эксперимент
				</div>
			</div>
			<div class="col-md-4">
				<img src="<?php echo ABV_THEME_URL; ?>/images/attension.png" alt="">
			</div>
		</div>
	</div>
	<div class="photo">
		<div class="title">
			фотогалерея
		</div>
		<div class="row">
			<div class="slider_photo">
				<div class="item"><a href="#"><img src="<?php echo ABV_THEME_URL; ?>/images/foto1.png" alt=""></a></div>
				<div class="item"><a href="#"><img src="<?php echo ABV_THEME_URL; ?>/images/foto1.png" alt=""></a></div>
				<div class="item"><a href="#"><img src="<?php echo ABV_THEME_URL; ?>/images/foto1.png" alt=""></a></div>
				<div class="item"><a href="#"><img src="<?php echo ABV_THEME_URL; ?>/images/foto1.png" alt=""></a></div>
			</div>
			<div class="see_all"><a href="#">смотреть все</a></div>
			<hr>
		</div>
	</div>
	<div class="new_articls">
		<div class="row">
			<div class="col-xs-12">
				<div class="title">
					новые статьи
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<a href="#">
					<div class="article">
						<div class="img_wrap">
							<img src="<?php echo ABV_THEME_URL; ?>/images/photo_post.png" alt="">
						</div>	
            <div class="wrap">
              <div class="title">
                Программа ориентирована на проживание в кругу светлых дней,  занятия соотносятся
              </div>
              <div class="data">
                <img src="<?php echo ABV_THEME_URL; ?>/images/clock.png" alt="">
                10 апреля 2016, 18:43
              </div>
              <div class="views">
                <img src="<?php echo ABV_THEME_URL; ?>/images/eye.png" alt="">
                25
              </div>
            </div>

            <div class="text">
             Красной нитью через все занятия проходит в первую очередь именно нравственное воспитание, семейный дом это дом, где 
           </div>
         </div>
       </a>
     </div>
     <div class="col-md-4">
      <a href="#">
        <div class="article">
          <div class="img_wrap">
            <img src="<?php echo ABV_THEME_URL; ?>/images/photo_post.png" alt="">
          </div>  
          <div class="wrap">
            <div class="title">
              Программа ориентирована на проживание в кругу светлых дней,  занятия соотносятся
            </div>
            <div class="data">
              <img src="<?php echo ABV_THEME_URL; ?>/images/clock.png" alt="">
              10 апреля 2016, 18:43
            </div>
            <div class="views">
              <img src="<?php echo ABV_THEME_URL; ?>/images/eye.png" alt="">
              25
            </div>
          </div>

          <div class="text">
           Красной нитью через все занятия проходит в первую очередь именно нравственное воспитание, семейный дом это дом, где 
         </div>
       </div>
     </a>
   </div>
   <div class="col-md-4">
    <a href="#">
      <div class="article">
        <div class="img_wrap">
          <img src="<?php echo ABV_THEME_URL; ?>/images/photo_post.png" alt="">
        </div>  
        <div class="wrap">
          <div class="title">
            Программа ориентирована на проживание в кругу светлых дней,  занятия соотносятся
          </div>
          <div class="data">
            <img src="<?php echo ABV_THEME_URL; ?>/images/clock.png" alt="">
            10 апреля 2016, 18:43
          </div>
          <div class="views">
            <img src="<?php echo ABV_THEME_URL; ?>/images/eye.png" alt="">
            25
          </div>
        </div>

        <div class="text">
         Красной нитью через все занятия проходит в первую очередь именно нравственное воспитание, семейный дом это дом, где 
       </div>
     </div>
   </a>
 </div>
</div>

<div class="row">
  <div class="show_all_articls">
    <a href="#">все статьи</a>
  </div>
</div>

</div>
</div>
</div>
<?php get_footer(); ?>
