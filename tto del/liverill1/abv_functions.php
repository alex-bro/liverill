<?php

// усі категоргії користувацького типу у асоц масив
function abv_get_all_category($taxonomy = 'products_cat'){
    $arr = array();
    $tax = '';
    global $wpdb;
    $pr = $wpdb->get_blog_prefix();
    $db =$wpdb->get_results("select * from {$pr}term_taxonomy where taxonomy = '{$taxonomy}'");
    foreach ($db as $item) {
        $tax .= $item->term_id.',';
    }
    $tax = substr($tax, 0, -1);
    wp_reset_query();
    $db = $wpdb->get_results("select * from {$pr}terms where term_id in ({$tax})");
    foreach ($db as $item) {
        $arr [$item->term_id]= $item->name;
    }
    wp_reset_query();
    return $arr;
}

// збирає айди категорії продукту у строку
function abv_get_terms_to_str($id, $term = 'products_cat'){
    $terms = get_the_terms($id, $term);
    if($terms){
        $str ='';
        foreach ($terms as $item){
            $str .= $item->term_id.' ';
        }
        $str = substr($str, 0, -1);
        return $str;
    }

}

//возвращает все ід и их название в виде ассоц массива
function abv_get_id_title($type = 'post'){
    $query = new WP_Query( array( 'post_type' => $type, 'orderby' => 'title', 'order' => 'ASC', 'nopaging'=>true ) );
    $arr = array();
    while ( $query->have_posts() ) {
        $query->the_post();
        $arr[get_the_ID()] = get_the_title();
    }
    wp_reset_query();
    return $arr;
}

//добавляет к айди их название магазиов
function abv_add_name($ids, $type = 'stores'){
    $query = new WP_Query( array( 'post_type' => $type, 'post__in'=> $ids, 'nopaging'=>true ) );
    $arr = array();
    while ( $query->have_posts() ) {
        $query->the_post();
        $arr[get_the_ID()] = get_the_title();
    }
    wp_reset_query();
    return $arr;
}



