<?php

if ( function_exists( 'add_theme_support' ) ) {
    add_theme_support( 'post-thumbnails' );
}

register_sidebar(array(
    'name' => 'Instagram про нас',
    'id' => 'instagram',
    'class' => '',
    'before_widget' => '',
    'after_widget' => ''));



add_filter('manage_posts_columns', 'abv_columns_names', 5);
add_action('manage_posts_custom_column', 'abv_add_thumbs', 5, 2);
add_filter('manage_pages_columns', 'abv_columns_names', 5);
add_action('manage_pages_custom_column', 'abv_add_thumbs', 5, 2);

function abv_columns_names($defaults){
    $defaults['abv_thumbs'] = 'Мініатюри';
    return $defaults;
}
function abv_add_thumbs($column_name, $id){
    if($column_name === 'abv_thumbs'){
        echo the_post_thumbnail(array(60,60));
    }
}

