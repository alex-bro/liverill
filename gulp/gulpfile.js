var gulp       = require('gulp'), // Подключаем Gulp
    sass         = require('gulp-sass'), //Подключаем Sass пакет,
    scss         = require('gulp-scss'), //Подключаем Scss пакет,
    browserSync  = require('browser-sync'), // Подключаем Browser Sync
    concat       = require('gulp-concat'), // Подключаем gulp-concat (для конкатенации файлов)
    uglify       = require('gulp-uglifyjs'), // Подключаем gulp-uglifyjs (для сжатия JS)
    cssnano      = require('gulp-cssnano'), // Подключаем пакет для минификации CSS
    rename       = require('gulp-rename'), // Подключаем библиотеку для переименования файлов
    del          = require('del'), // Подключаем библиотеку для удаления файлов и папок
    imagemin     = require('gulp-imagemin'), // Подключаем библиотеку для работы с изображениями
    pngquant     = require('imagemin-pngquant'), // Подключаем библиотеку для работы с png
    cache        = require('gulp-cache'), // Подключаем библиотеку кеширования
    autoprefixer = require('gulp-autoprefixer'),// Подключаем библитеку для автоматического добавления префиксов
    plumber      = require('gulp-plumber');
    var path_theme = '../wp-content/themes/abv_liverill/';

gulp.task('sass', function(){ // Создаем таск Sass
    return gulp.src(['app/sass/**/*.sass']) // Берем источник
        .pipe(sass()) // Преобразуем Sass в CSS посредством gulp-sass
        .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true })) // Создаем префиксы
        .pipe(gulp.dest('app/css')) // Выгружаем результата в папку app/css
        .pipe(browserSync.reload({stream: true})) // Обновляем CSS на странице при изменении
});

gulp.task('scss', function(){ // Создаем таск Sass
    return gulp.src(['app/scss/**/*.scss']) // Берем источник
        .pipe(plumber())
        .pipe(scss()) // Преобразуем Sass в CSS посредством gulp-sass
        .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true })) // Создаем префиксы
        .pipe(gulp.dest('app/css')) // Выгружаем результата в папку app/css
        .pipe(browserSync.reload({stream: true})) // Обновляем CSS на странице при изменении
});



gulp.task('browser-sync', function() { // Создаем таск browser-sync
    browserSync({ // Выполняем browserSync
        server: { // Определяем параметры сервера
            baseDir: 'app' // Директория для сервера - app
        },
        notify: false // Отключаем уведомления
    });
});

gulp.task('scripts', function() {
    return gulp.src([ // Берем все необходимые библиотеки
        'app/libs/jquery/dist/jquery.min.js', // Берем jQuery
        //'app/libs/magnific-popup/dist/jquery.magnific-popup.min.js', // Берем Magnific Popup
        'app/libs/bootstrap-sass/assets/javascripts/bootstrap.min.js',
        'app/libs/masonry/dist/masonry.pkgd.min.js',
        //'app/libs/fullpage.js/dist/jquery.fullpage.min.js',
        //'app/libs/pagePiling.js/jquery.pagepiling.min.js',
        //'app/libs/owl-carousel/owl-carousel/owl.carousel.min.js',
        'app/libs/jquery-ui/jquery-ui.min.js',
        'app/libs/slick-carousel/slick/slick.min.js',
        ])
        .pipe(concat('libs.min.js')) // Собираем их в кучу в новом файле libs.min.js
        .pipe(uglify()) // Сжимаем JS файл
        .pipe(gulp.dest('app/js')); // Выгружаем в папку app/js
});

gulp.task('css-libs', [
    //'sass',
    'scss',
    ], function() {
    return gulp.src([
        'app/css/_bootstrap.css',
        'app/libs/jquery-ui/jquery-ui.min.css',
        'app/libs/font-awesome/css/font-awesome.min.css',
        //'app/libs/fullpage.js/dist/jquery.fullpage.min.css',
        //'app/libs/pagePiling.js/jquery.pagepiling.css',
        //'app/libs/owl-carousel/owl-carousel/owl.carousel.css',
        'app/libs/slick-carousel/slick/slick.css',
        //'app/libs/slick-carousel/slick/slick-theme.css',
        ]) // Выбираем файл для минификации
        //.pipe(concat('lib.css'))
        //.pipe(cssnano()) // Сжимаем
        //.pipe(rename({suffix: '.min'})) // Добавляем суффикс .min
        .pipe(gulp.dest('app/css')); // Выгружаем в папку app/css
});
gulp.task('watch', ['browser-sync', 'css-libs', 'scripts'], function() {
    gulp.watch('app/scss/**/*.scss', ['scss']); // Наблюдение за sass файлами в папке scss
    //gulp.watch('app/sass/**/*.sass', ['sass']); // Наблюдение за sass файлами в папке sass
    gulp.watch('app/css/*.css', browserSync.reload); // Наблюдение за css файлами
    gulp.watch('app/*.html', browserSync.reload); // Наблюдение за HTML файлами в корне проекта
    gulp.watch('app/js/**/*.js', browserSync.reload);   // Наблюдение за JS файлами в папке js


});

var watcher_css = gulp.watch('app/css/**/*.css');
watcher_css.on('change', function(event) {
    var copyCss = gulp.src(event.path)
        .pipe(gulp.dest(path_theme+'css'))
    console.log('Saved '+event.path)
});

var watcher_js = gulp.watch('app/js/**/*.js');
watcher_js.on('change', function(event) {
    var copyJs = gulp.src(event.path)
        .pipe(gulp.dest(path_theme+'js'))
    console.log('Saved '+event.path)
});

var watcher_img = gulp.watch('app/img/**/*');
watcher_img.on('change', function(event) {
    var copyImg = gulp.src(event.path)
        .pipe(gulp.dest(path_theme+'img'))
    console.log('Saved '+event.path)
});

gulp.task('clean', function() {
    return del.sync('dist'); // Удаляем папку dist перед сборкой
});

gulp.task('img', function() {
    return gulp.src('app/img/**/*') // Берем все изображения из app
        .pipe(cache(imagemin({  // Сжимаем их с наилучшими настройками с учетом кеширования
            interlaced: true,
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        })))
        .pipe(gulp.dest('dist/img')); // Выгружаем на продакшен
});

gulp.task('copy_css',function(){
    //var path = '../wp-content/themes/abv_liveril/';
    var copyCss = gulp.src('app/css/main.css')
        .pipe(gulp.dest(path_theme+'test'))
});

gulp.task('build', ['clean', 'img', 'scss', 'scripts'], function() {

    var buildCss = gulp.src([ // Переносим библиотеки в продакшен
        'app/css/_bootstrap.css',
        'app/css/libs.min.css',
        'app/libs/font-awesome/css/font-awesome.min.css',
        'app/css/main.css',
        ])
    .pipe(gulp.dest('dist/css'))

    var buildFonts = gulp.src(['app/fonts/**/*', 'app/libs/font-awesome/fonts/*.ttf']) // Переносим шрифты в продакшен
    .pipe(gulp.dest('dist/fonts'))

    var buildJs = gulp.src('app/js/**/*') // Переносим скрипты в продакшен
    .pipe(gulp.dest('dist/js'))

    var buildHtml = gulp.src('app/*.html') // Переносим HTML в продакшен
    .pipe(gulp.dest('dist'));

});

gulp.task('clear', function () {
    return cache.clearAll();
})

gulp.task('default', ['watch']);