jQuery(document).ready(function($) {
    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        items:1,
        nav:true,
        navText: ['<i class="fa fa-chevron-right fa-2x" aria-hidden="true"></i>',
            '<i class="fa fa-chevron-left fa-2x" aria-hidden="true"></i>'],

    });
    $('.slider_photo').owlCarousel({
        loop:true,
        margin:10,
        items:4,
        nav:true,
        navText: ['<i class="fa fa-chevron-right " aria-hidden="true"></i>',
            '<i class="fa fa-chevron-left " aria-hidden="true"></i>'],

    });
});

